; Kalatheme Power Makefile

api = 2
core = 7.x

; Kalatheme Helpers

projects[elements][version] = 1.4
projects[elements][subdir] = contrib

projects[icon][version] = 1.x-dev
projects[icon][subdir] = contrib

projects[magic][version] = 1.5
projects[magic][subdir] = contrib

projects[speedy][version] = 1.12
projects[speedy][subdir] = contrib

projects[views_bootstrap][version] = 3.1
projects[views_bootstrap][subdir] = contrib

projects[views_responsive_grid][version] = 1.3
projects[views_responsive_grid][subdir] = contrib